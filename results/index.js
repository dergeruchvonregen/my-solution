#!/usr/bin/env node

const amqp = require("amqplib/callback_api");
const { Buffer } = require("buffer");

amqp.connect("amqp://0.0.0.0:5672", function (er, connection) {
  if (er) throw er;

  connection.createChannel(function (er, channel) {
    if (er) throw er;

    var messages = [];
    var biggest = { rand: "", sequence_number: "" };

    channel.consume("rand", function (message) {
      const data = JSON.parse(message.content.toString());
      messages.push(data);
      messages.length === 101 && messages.shift();

      if (data.rand > biggest.rand) {
        biggest = data;
      }

      if (data.sequence_number - biggest.sequence_number === 100) {
        biggest = messages.reduce((past, number) =>
          past.rand > number.rand ? past : number
        );
      }

      // returning response
      channel.sendToQueue(
        "solution",
        Buffer.from(JSON.stringify({ ...data, running_max: biggest.rand }))
      );
    });
  });
});
