# My solution is [here](https://gitlab.com/dergeruchvonregen/my-solution/-/tree/main/results).

```bash
docker-compose up -d # starts the docker-compose services and begins producing messages
# wait a second or two
docker-compose ps # ensure that the rabbitmq and validator services are "up". The number generator will terminate once it has produced its allocated number of messages.
docker-compose logs -f # Check if the rabbitmq broker is up and the other services have `CONNECTED`

# run your own solution - you'll know better what to do here (hopefully)
```

It is a good idea to stop the `docker-compose` when you have tested your script, as it will restart the containers on every boot of your machine.
Unless you specify a `NUMBER_OF_MESSAGES` environment variable in the docker-compose it will produce messages for ever.

Please do so by running:

```bash
docker-compose down
```